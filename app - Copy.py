from flask import Flask, flash, redirect, render_template, request, session, abort,url_for,send_file,send_from_directory,send_file
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine,and_
from flask_socketio import SocketIO, send

import os
import nltk
import gensim
import numpy as np
import pandas as pd
from scipy import spatial
from itertools import chain
from nltk.stem import WordNetLemmatizer
import matplotlib.pyplot as plt
from nltk.corpus import stopwords
import re
import codecs
from sklearn import feature_extraction
from nltk.stem.snowball import SnowballStemmer
import mpld3
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans
from sklearn.metrics import adjusted_rand_score
from nltk.tokenize import sent_tokenize
from sklearn.metrics.pairwise import cosine_similarity
import subextract_final1
import collections

nested_dictionary=collections.defaultdict(dict)
diction = {}
dictio = {}
dictionary_for_answering = {}
true_k = 15
vectorizer = TfidfVectorizer(stop_words='english')
model = KMeans(n_clusters=true_k, init='k-means++', max_iter=100,n_init=1)


def read():

	global nested_dictionary
	all_content=[]
	nested_dictionary=subextract_final1.ppsub()
	topics=list(nested_dictionary.keys())
	subtopics=[]
	for i in topics:
		t=list(nested_dictionary[i].keys())
		subtopics=subtopics+t
		for j in t:
			all_content.append(nested_dictionary[i][j])
				
	subtopics=list(subtopics)
	subtopics=set(subtopics)
	subtopics=list(subtopics)
	#print(all_content)
			
	z=topics+subtopics+all_content


	X = vectorizer.fit_transform(z)
	
	model.fit(X)
	print("Top terms per cluster:")
	order_centroids = model.cluster_centers_.argsort()[:, ::-1]
	terms = vectorizer.get_feature_names()
	for i in range(true_k):
		print("Cluster %d:" % i),
		for ind in order_centroids[i, :10]:
			print(' %s' % terms[ind]),
		print
	print("\n")
	#print(model.labels_)
	print("\n")
	print("Prediction")
	
	for clusno in range(0, true_k):
		diction.setdefault(clusno, [])
		
	for clusno in range(0, true_k):
		dictio.setdefault(clusno, [])
		
	ind = 0
	
	for i in topics:
		sv1=[i]
		svdoc=sent_tokenize(i)
		Y=vectorizer.transform(svdoc)
		prediction = model.predict(Y)
		pred=prediction[0]
		dictio[pred].append(sv1)
		
	for key in dictio:
		print(str(key)+"-------------------"+str(dictio[key]))


#---------------------------------------------------------------------------
#---------------------------------------------------------------------------



engine2 = create_engine('sqlite:///students.sqlite3', echo=True)
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///students.sqlite3'
app.config['SQLALCHEMY_BINDS']={'history': 'sqlite:///chat_history.sqlite3', 'support_system' : 'sqlite:///support_system_queries.sqlite3','answered_support' : 'sqlite:///answered_support_system_queries.sqlite3'}
app.config['SECRET_KEY'] = "random string"

global_context=""
i=0
msg=""
count=0
POST_GLOBAL_USERNAME=""
db = SQLAlchemy(app)
socketio = SocketIO(app)
APP_ROOT = os.path.dirname(os.path.abspath(__file__))

class file_class:
	def __init__(self,id2,file_name):
		self.id2=id2
		self.file_name=file_name
	
	
class students(db.Model):
	id = db.Column('student_id', db.Integer, primary_key = True)
	name = db.Column(db.String(100))
	passw = db.Column(db.String(50))
	person_name= db.Column(db.String(100))
   
	def __init__(self,name, passw,person_name):
		self.name = name
		self.passw = passw
		self.person_name=person_name
	

class support_system_queries(db.Model):
	__bind_key__ ='support_system'
	id = db.Column('query_id', db.Integer, primary_key = True)
	question = db.Column(db.String(200))
	answer = db.Column(db.String(2000))
	cust_id = db.Column(db.String(100))
	support_name=db.Column(db.String(100))
	support_date=db.Column(db.String(50))
	
	def __init__(self,id,question, answer,cust_id,support_name,support_date):
		self.id = id
		self.question = question
		self.answer = answer
		self.cust_id = cust_id
		self.support_name=support_name
		self.support_date=support_date

class answered_support_system_queries(db.Model):
	__bind_key__ ='answered_support'
	id = db.Column('query_id', db.Integer, primary_key = True)
	question = db.Column(db.String(200))
	answer = db.Column(db.String(2000))
	
	
	def __init__(self,id,question, answer):
		self.id = id
		self.question = question
		self.answer = answer

		
class chat_history(db.Model):
	__bind_key__ ='history'
	id = db.Column('message_id', db.Integer, primary_key = True)
	cust_id = db.Column(db.String(100))
	ques = db.Column(db.String(150))
	ans = db.Column(db.String(2000))
	context=db.Column(db.String(2000))
   
	def __init__(self,id,cust_id,ques, ans,context):
		self.id = id
		self.cust_id = cust_id
		self.ques=ques
		self.ans=ans
		self.context=context

''' 							ALL METHODS
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
'''	   
@socketio.on('message')
def handleMessage(msg):
	global i,count,vectorizer,model,dictio,dictionary_for_answering,global_context,nested_dictionary
	response=""
	print("///////////////////"+str(msg)+"$$$$$$$555555555555555555555555$$$$$$$$$")
	query=msg
	trial_query=msg.split('__')
	if(trial_query[0]=='&&'):
		query=trial_query[1]
	elif(trial_query[0]=='##'):
		query=trial_query[1]
	else:
		query=msg

	query_within_list=[query]
	Y = vectorizer.transform(query_within_list)
	predic = model.predict(Y)
	predicti=predic[0]
	
	print("%%%%%%%%%%%%"+str(predicti)+"%%%%%%%%%%")
	
	stri=""
	max=0
	for ls in dictio[predicti]:
		#ls[0]=ls[0].lower()
		TEMP=vectorizer.transform(ls)
		dist = cosine_similarity(Y,TEMP)
		print(dist)
		if dist[0]>max:
			max=dist[0]
			stri=ls[0]
	print(stri)
	print("*******"+str(max))
	
	if(stri==""):
		response="None"
	else:
		for i in nested_dictionary[stri]:
			#print("-------------------In here"+nested_dictionary[stri][i])
			response=response+nested_dictionary[stri][i]
	#except IndexError:
		#print("-------------------First response")
		#response="None"
	
	max1=0
	stri1=""
	
	
	try:
		for ls1 in nested_dictionary[stri]:
			ls2=ls1.replace('\n','')
			print(ls2)
			lss=[ls2]
			TEMP1=vectorizer.transform(lss)
			dist1 = cosine_similarity(Y,TEMP1)
			print(dist1)
			if dist1[0]>max1:
				max1=dist1[0]
				stri1=ls1
			if(max1>(max/2)):
				response=nested_dictionary[stri][stri1]    
	except IndexError:
		response="None"
	
	data_received_str = msg.split("__")
	context_str=str(data_received_str)
	
	
	if(data_received_str[0]=="&&"):
		global_context=data_received_str[1]
		query2 = chat_history.query.order_by(chat_history.id.desc()).first()
		if(str(query2)==None):
			count=0
		else:
			count= int(str(query2.id))+1
		
		#if no response...insert into support database
		if str(response)=="None":		
			support_system_instance=support_system_queries(id=count,cust_id=POST_GLOBAL_USERNAME,question=query,answer="None",support_date="",support_name="")
			db.session.add(support_system_instance)
			db.session.commit()
		
		chat_history_instance=chat_history(id=count,cust_id=POST_GLOBAL_USERNAME,ques=global_context,ans=response,context=global_context)
		db.session.add(chat_history_instance)
		db.session.commit()
		send(response, broadcast=True)
		chatbot()
	
	
	
	elif(data_received_str[0]=="@@"):			#if context is changed
		global_context=data_received_str[1]
		chatbot()
	
	elif(data_received_str[0]=="##"):		#if the support team responds
	
		user_id_split=data_received_str[1].split("_")
		user_id=user_id_split[1]

		update_answer=chat_history.query.filter_by(id=user_id).first()
		update_answer.ans=str(data_received_str[3])
		
		chat_history_instance_for_answered=answered_support_system_queries(id=user_id,question=data_received_str[2],answer=update_answer.ans)
		db.session.add(chat_history_instance_for_answered)
		db.session.commit()
		
		support_system_queries.query.filter_by(id=user_id).delete()
		db.session.commit()
		
	else:	#if the chatbot responds
		query2 = chat_history.query.order_by(chat_history.id.desc()).first()
		if(str(query2)==None):
			count=0
		else:
			count= int(str(query2.id))+1	
		
		#if no response...insert into support database
		if str(response)=="None":
			print("///////////////////////////"+query+"-------------------------")
			support_system_instance=support_system_queries(id=count,cust_id=POST_GLOBAL_USERNAME,question=query,answer="None",support_date="",support_name="")
			db.session.add(support_system_instance)
			db.session.commit()
		
		chat_history_instance=chat_history(id=count,cust_id=POST_GLOBAL_USERNAME,ques=query,ans=response,context=global_context)
		db.session.add(chat_history_instance)
		db.session.commit()
		send(response, broadcast=True)

'''							CHATBOT SCREEN
#---------------------------------------------------------------------
#---------------------------------------------------------------------
'''
@app.route('/')
def main_page():
	if session.get('logged_in'):
		msg="CHAT"
	else:
		msg="LOGIN"
	return render_template('index.html',show_menu=msg)
 
@app.route('/home_page')
def home_page():
	session['logged_in'] = False
	return render_template('index.html',show_menu="Login")

@app.route('/home')
def home():
	if not session.get('logged_in'):
		return render_template('login.html',msg=msg)
	else:
		return chatbot()

@app.route("/chatbot")
def chatbot():
	global global_context
	chats=db.session.query(chat_history).filter(chat_history.cust_id.in_([POST_GLOBAL_USERNAME]) )
	context_list=[]
	
	for value in chats:
		context_list.append(value.context)
	
	context_set=list(set(context_list))
	
	if global_context=="":
		global_context=str(context_set[0])
	
	context_specific_chat=chats.filter(chat_history.context.in_([global_context]))
	display_current_context=""
	for values in context_specific_chat:
		display_current_context=str(values.ques)
		print("////////$$$$$$$$"+str(values)+"YOLO"+str(values.ques))
		break
	
	#print("////////////////////////////")
	#for values in chats:
		#if (str(value.context)==global_context):
	#	print(str(values))	
	
	session['logged_in'] = True
	return render_template('chatbot.html',msg=msg,myChat=chats,username_show=POST_GLOBAL_USERNAME,myContext=context_set,context_specific_chat=context_specific_chat,display_current_context=display_current_context)
	
'''							LOGIN AND LOGOUT
#----------------------------------------------------------------------	
#----------------------------------------------------------------------
'''
@app.route('/login', methods=['POST'])
def do_admin_login():
	global msg,POST_GLOBAL_USERNAME
	msg=""
	POST_USERNAME = str(request.form['username'])
	POST_PASSWORD = str(request.form['password'])

	if len(POST_USERNAME)==8 and len(POST_PASSWORD)==8 :
		Session = sessionmaker(bind=engine2)
		s = Session()
		query = s.query(students).filter(students.name.in_([POST_USERNAME]), students.passw.in_([POST_PASSWORD]) )
		result = query.first()
		if result:	#if result is found
			if POST_USERNAME == "admin789" :
				session['logged_in'] = True
				POST_GLOBAL_USERNAME = POST_USERNAME
				msg="Welcome admin"
				#return admin_page()
				print("---------------------------------------------------")
				return admin_option()
			elif POST_USERNAME=="support0":
				session['logged_in'] = True
				POST_GLOBAL_USERNAME=POST_USERNAME
				msg="welcome support staff"
				return support_page()
			else:
				session['logged_in'] = True
				POST_GLOBAL_USERNAME = POST_USERNAME
				msg="Welcome"+str(POST_USERNAME)
		else:
			msg="wrong credentials"
			
	else:
		msg=("Wrong char length")
	return home()

@app.route("/logout")
def logout():
    session['logged_in'] = False
    return main_page()

	
	
@app.route("/admin_option")
def admin_option():
	if not session.get('logged_in'):
		return render_template('login.html',msg=msg)
	else:
		return render_template('services.html')

'''							NEW USER REGISTER
#-------------------------------------------------------------------------  
#-------------------------------------------------------------------------
'''
@app.route("/new_reg", methods=['GET','POST'])
def new_reg():
	global msg
	msg=""
	if request.method == 'POST':
		POST_USERNAME = str(request.form['name'])
		POST_PASSWORD = str(request.form['passw'])
		POST_USER_FULLNAME=str(request.form['person_name'])
		
		if len(POST_USERNAME)==8 and len(POST_PASSWORD)==8 :
			Session = sessionmaker(bind=engine2)
			s = Session()
			query = s.query(students).filter(students.name.in_([POST_USERNAME]) )
			result = query.first()
			
			if result:	#if result is found
				msg="record_exist"		
			else:
				msg="success"
				student = students(name=request.form['name'], passw=request.form['passw'], person_name=request.form['person_name'])
				db.session.add(student)
				db.session.commit()
				return redirect(url_for('home_page'))
		else:
			msg="fill_all"
	return render_template('new_reg.html',msg=msg,username_show="ADMIN")

'''							ADMIN METHODS
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
'''	
@app.route('/admin')
def admin_page():
	data2=[]
	counter=1
	for root, dirs, files in os.walk("./PDFs/"):  
		for filename in files:
			data2.append(file_class(counter,filename))
			counter=counter+1
	return render_template("admin_upload.html",msg="",class_data=data2)

	
@app.route('/support')
def support_page():
	support_queries=db.session.query(support_system_queries)
	session['logged_in'] = True
	return render_template('support_team.html',msg=msg,myChat=support_queries)
	
	
#for admin file download
@app.route('/download')
def download():
	data2=[]
	counter=1
	for root, dirs, files in os.walk("./PDFs/"):  
		for filename in files:
			data2.append(file_class(counter,filename))
			counter=counter+1				
	return render_template('download.html',class_data=data2)
 
@app.route('/down_file/<path:filename>', methods=['GET', 'POST'])
def down_file(filename):
	APP_ROOT=os.path.dirname(os.path.abspath(__file__))
	APP_ROOT=APP_ROOT+'\PDFs\\'
	return send_from_directory(APP_ROOT, filename=filename)

@app.route('/upload',methods=['POST'])
def upload():
	target = os.path.join(APP_ROOT, 'PDFs/')

	if not os.path.isdir(target):
		os.mkdir(target)

	for file in request.files.getlist("inputfile"):
		if file.filename!="":                        
			filename = file.filename
			extension=os.path.splitext(filename)[1]
			if (extension!=".pdf"):
				msg='wrong file type..only PDF supported'
			else:
				destination = "/".join([target, filename])
				print(destination)
				file.save(destination)
				msg="FILE UPLOADED"
		else:
			msg="select a file"
						
	data2=[]
	counter=1
	for root, dirs, files in os.walk("./PDFs/"):  
		for filename in files:
			data2.append(file_class(counter,filename))
			counter=counter+1					
	return render_template("admin_upload.html",msg=msg,class_data=data2)

@app.route('/about_us')
def about_us():
	if session.get('logged_in'):
		msg="CHAT"
	else:
		msg="LOGIN"
	#return render_template('index.html',show_menu=msg)
	return render_template("about_us.html",show_menu=msg)
	

'''
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
'''
	
if __name__ == "__main__":
	db.create_all()
    #Main function to send a file to the pre process data function
	#model, dict_quest, dict_ans, dataframe = preprocess_data_from_corpus('training.xlsx') #Provide your file name here
	read()
	socketio.run(app)
        
	
			
