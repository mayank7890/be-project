import requests
from bs4 import BeautifulSoup
import re
import numpy as np
import collections

def ppsub():
    url = 'https://www.cisco.com/c/en/us/td/docs/security/physical_security/video_surveillance/network/vsm/release_notes/7_9_0/vsm_rn_7_9_0.html#pgfId-303347'
    response = requests.get(url)
    html = response.content
    #print(html)

    soup = BeautifulSoup(html, 'html.parser')
    main_keys=[];                                                      #Initial keys
    main_values=[];                                                    #Initial values 
    sorted_keys=[];                                                    #Final keys
    sorted_values=[];                                                  #Final values
    link1=soup.find_all("h3",class_="p_H_Head2")

    ###################################################################################
    #for i in link1:
    #	main_keys.append(i.contents)
    #for k in main_keys:
    #	for p in range (0,len(k)):
    #		if(k[p].find('</a>')==-1 and k[p]!='\n' and k[p].find('</h3>')==-1):
    #			sorted_keys.append(k[p])
    #total keys 41
    ###################################################################################

    link2=soup.find("blockquote")
    link3=link2.find_all("div")
    key=""
    subkey=""
    temp_vals=[];
    main_dict={};
    #nested_dict={};
    vals=[];
    temp=[];
    nested_dict=collections.defaultdict(dict)
    k=0
    p=""
    q=""
    for i in  link3:
            temp=[]
            subkey=""
            string=""
            temp_vals=[]
            if (k>4):
                    if(i.find("h3",class_="p_H_Head2")):
                            key1=i.h3.text
                            key=key1.replace('\n','')
                            #print(key)
                            nested_dict[key]={}
                            nested_dict[key].setdefault("Introduction","")
                                            
                                            
                    if(i.find("p",class_="pBl_BlockLabel")):
                            m=i.find_all("p",class_="pBl_BlockLabel")
                            vals=[];
                            for l in m:
                                    vals.append(l.text)
                            #print(key+"--------"+str(len(vals)))
                            strn=''.join(vals)
                            #main_dict[str(key)]=strn
                            
                            for j in i:
                                    if(j!='\n' and (j.text in vals)):
                                            if(subkey==""):
                                                    subkey="Introduction"
                                            string=''.join(temp)
                                            #print(string)
                                            #print(key+"====== "+subkey)
                                            nested_dict[key][subkey]=string
                                            temp=[]
                                            subkey=j.text
                                            nested_dict[key].setdefault(subkey)
                                            #print(p)
                                            #print("********************************")
                                                            
                                            #print(temp)
                                            #strn1=''.join(temp)
                                            #nested_dict[str(key)][str(p)]=strn1
                                            #print("--------------------------------")
                                            #temp=[];
                                    elif(j!='\n' and subkey!="NULL" and j.text!=key1 and not j.find("table")):
                                            #print(j.text)
                                            temp.append(j.text)
                            string=''.join(temp)
                            nested_dict[key][subkey]=string
                    #elif(not i.find("p",class_="pBl_BlockLabel")):
                    else:
                            if(i.find_all("p")):
                                    temp_vals.append(i.contents)
                            #print(temp_vals)

                            for q in range(0,len(temp_vals)):
                                    for j in temp_vals[q]:
                                            if(j!='\n' and j.text!=key1 and not j.find("table")):
                                                    #print("@@@@@@@@@@@@@@@@@@")
                                                    #print(j.text)
                                                    temp.append(j.text)
                            string=''.join(temp)
                            #print(key+"**********************")
                            #print(string)
                            if(nested_dict[key]["Introduction"]==""):
                                    nested_dict[key]["Introduction"]=string
            k+=1
    '''
                            for j in i:
                                    print(j)
                                    print("@@@@@@@@")
                                    print(j.text)
                                    print("\n\n")
                                    if(j!='\n' and not j.find("table")):
                                            #print(j.text)
                                            temp.append(j.text)
                            string=''.join(temp)
                            nested_dict[key]["Introduction"]=string
                                                    
            
    #print(nested_dict)
    for key in nested_dict:
            print(key)
            print("------------------------")
            print(nested_dict[key])
    '''
    temp=[]
    keyh2=""
    subkey=""
    link4=soup.find_all("div")
    count=0
    for o in link4:
        temp=[]
        subkey=""
        string=""
        temp_vals=[]
        if(o.find("h2",class_="p_H_Head1")):
            m=o.h2.text
            count+=1
            if(count>9):
                #print(m)
                keyy=m
                keyh2=keyy.replace('\n','')
                nested_dict[keyh2]={}
                nested_dict[keyh2].setdefault("Introduction","")

                if(o.find("p",class_="pBl_BlockLabel")):
                    b=o.find_all("p",class_="pBl_BlockLabel")
                    vals1=[];
                    for q in b:
                        vals1.append(q.text)
                    #print(vals1)
                    #print(keyy+"--------"+str(len(vals1)))
                    strn=''.join(vals1)
                    #main_dict[str(key)]=strn
                                
                    for j in o:
                        if(j!='\n' and (j.text in vals1)):
                            if(subkey==""):
                                subkey="Introduction"
                            string=''.join(temp)
                            #print("[[[[[[[[["+string)
                            #print(keyy+"====== "+subkey)
                            nested_dict[keyh2][subkey]=string
                            temp=[]
                            subkey=j.text
                            nested_dict[keyh2].setdefault(subkey)
                            #print(p)
                            #print("********************************")
                            #print(temp)
                            #strn1=''.join(temp)
                            #nested_dict[str(key)][str(p)]=strn1
                            #print("--------------------------------")
                            #temp=[];
                        elif(j!='\n' and subkey!="NULL" and j.text!=keyy and not j.find("table")):
                            #print("!!!!!!!!!!!!")
                            #print(j.text)
                            temp.append(j.text)
                    string=''.join(temp)
                    if(keyh2=="Caveats" or keyh2=="Understanding the Cisco VSM Software Types"):
                            nested_dict[keyh2]["Introduction"]=string        
                    else:
                            nested_dict[keyh2][subkey]=string
                    #elif(not i.find("p",class_="pBl_BlockLabel")):
                else:
                    if(i.find_all("p")):
                        temp_vals.append(i.contents)
                        #print(temp_vals)

                        for q in range(0,len(temp_vals)):
                            for j in temp_vals[q]:
                                if(j!='\n' and j.text!=key1 and not j.find("table")):
                                    #print("@@@@@@@@@@@@@@@@@@")
                                    #print(j.text)
                                    temp.append(j.text)
                        string=''.join(temp)
                        #print(key+"**********************")
                        #print(string)
                    if(nested_dict[key]["Introduction"]==""):
                            nested_dict[key]["Introduction"]=string    
        
    return nested_dict
    
