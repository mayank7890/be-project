from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans
from sklearn.metrics import adjusted_rand_score
 
 
def mapping_for_questions(question_list):  
	vectorizer_for_question = TfidfVectorizer(stop_words='english')
	X1 = vectorizer_for_question(question_list)
	 
	true_k1 = 4
	model_for_question = KMeans(n_clusters=true_k1, init='k-means++', max_iter=100, n_init=1)
	model_for_question.fit(X1)
	 
	print("Top terms per cluster:")
	order_centroids = model_for_question.cluster_centers_.argsort()[:, ::-1]
	terms = vectorizer_for_question.get_feature_names()
	for i in range(true_k):
		print("Cluster %d:" % i),
		for ind in order_centroids[i, :10]:
			print(' %s' % terms[ind]),
		print
	#print("\n")
	#print(model_for_question_)
	#print("\n")
	#print("Prediction")
	
	cluster_dictionary_for_question={}
	
	for clusno1 in range(0, 5):
		cluster_dictionary_for_question.setdefault(clusno1, [])
	
	for i in question_list:
		qu1=[i]
		qudoc=sent_tokenize(i)
		Y1=vectorizer_for_question.transform(svdoc)
		prediction1 = model_for_question.predict(Y1)
		pred1=prediction1[0]
		cluster_dictionary_for_question[pred1].append(qu1)
	
	
	response=""
	match=False
	maximum=0
	strin=""
	
	for itr in question_list:
		itr_list=[itr]
		trans=vectorizer_for_question.transform(itr_list)
		distance=cosine_similarity(Y,trans)
		print(distance)
		if distance[0]>maximum:
			maximum=distance[0]
			strin=itr
	
	if(maximum>0.4):
		response=strin
		
	return response