#Our Projects ReadMe.md
1. The basic requirement is python3.

2. Install the following requirements using the command:
	pip3 install library_name.

3. Requirements are:
	Werkzeug
	Jinja2
	MarkupSafe
	ItsDangerous
	Click
	virtualenv
	Flask
	flask_sqlalchemy
	flask_socketio
	nltk 
	gensim
	numpy
	pandas
	scipy
	sklearn
	matplotlib
	regex
	mpld3
	beautifulsoup4

4. Install python3-tk using apt-get.

5. Open Python3 on command line and import nltk and run the command nltk.download("punkt").

6. Now, go into your project directory and run the command python3 app.py.