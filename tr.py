import os
import nltk
import gensim
import numpy as np
import pandas as pd
from scipy import spatial
from itertools import chain
from nltk.stem import WordNetLemmatizer
import matplotlib.pyplot as plt
from nltk.corpus import stopwords
import re
import codecs
from sklearn import feature_extraction
from nltk.stem.snowball import SnowballStemmer
import mpld3
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans
from sklearn.metrics import adjusted_rand_score
from nltk.tokenize import sent_tokenize
from sklearn.metrics.pairwise import cosine_similarity
import subextract_final

dictionary_for_answering={}

all_content=[]
nested_dictionary=subextract_final.ppsub()
#print(nested_dictionary)
topics=list(nested_dictionary.keys())
subtopics=[]
for i in topics:
    t=list(nested_dictionary[i].keys())
    subtopics=subtopics+t
    for j in t:
        all_content.append(nested_dictionary[i][j])
            
subtopics=list(subtopics)
subtopics=set(subtopics)
subtopics=list(subtopics)
#print(all_content)
        
z=topics+subtopics+all_content


vectorizer = TfidfVectorizer(stop_words='english')
X = vectorizer.fit_transform(z)
#print(X)

true_k= 15
model = KMeans(n_clusters=true_k, init='k-means++', max_iter=100, n_init=1)
model.fit(X)

print("Top terms per cluster:")
order_centroids = model.cluster_centers_.argsort()[:, ::-1]
terms = vectorizer.get_feature_names()
for i in range(true_k):
    print("Cluster %d:" % i),
    for ind in order_centroids[i, :10]:
        print(' %s' % terms[ind]),
    print
print("\n")
#print(model.labels_)
print("\n")
print("Prediction")
 

#prediction = model.predict(Y)
#print(prediction)

diction={}
dictio={}
#lst = [[] for _ in range(15)]
for clusno in range(0,true_k):
        diction.setdefault(clusno,[])

for clusno in range(0,true_k):
        dictio.setdefault(clusno,[])

ind=0

for i in topics:
        sv1=[i]
        svdoc=sent_tokenize(i)
        Y=vectorizer.transform(svdoc)
        prediction = model.predict(Y)
        pred=prediction[0]
        dictio[pred].append(sv1)

for key in dictio:
    print(str(key)+"-------------------"+str(dictio[key]))

response=""
query=input("Enter the query: ")
query_within_list=[query]
Y = vectorizer.transform(query_within_list)
predic = model.predict(Y)
predicti=predic[0]

print("@@@@@@@@@"+str(predicti))
stri=""
max=0

for ls in dictio[predicti]:
    #ls[0]=ls[0].lower()
    TEMP=vectorizer.transform(ls)
    dist = cosine_similarity(Y,TEMP)
    print(dist)
    if dist[0]>max:
        max=dist[0]
        stri=ls[0]
print(stri)
print("*******"+str(max))


try:
    for i in nested_dictionary[stri]:
        response=response+nested_dictionary[stri][i]
except IndexError:
    response="Invalid"
max1=0
stri1=""

try:
    for ls1 in nested_dictionary[stri]:
        ls2=ls1.replace('\n','')
        print(ls2)
        lss=[ls2]
        TEMP1=vectorizer.transform(lss)
        dist1 = cosine_similarity(Y,TEMP1)
        print(dist1)
        if dist1[0]>max1:
            max1=dist1[0]
            stri1=ls1
        if(max1>(max/2)):
            response=nested_dictionary[stri][stri1]    
except IndexError:
    response="Invalid"
print(response)


